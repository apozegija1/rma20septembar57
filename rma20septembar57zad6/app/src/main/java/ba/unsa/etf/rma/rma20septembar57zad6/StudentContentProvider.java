package ba.unsa.etf.rma.rma20septembar57zad6;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class StudentContentProvider extends ContentProvider {
    public static final int ALL_ROWS = 1;
    public static final int ONE_ROW = 2;
    public static UriMatcher uM = null;

    StudentDBOpenHelper dbOpenHelper = new StudentDBOpenHelper(getContext(), StudentDBOpenHelper.DATABASE_NAME, null, StudentDBOpenHelper.DATABASE_VERSION);;
    public static final String AUTHORITY = "ba.unsa.etf.rma.rma20septembar57zad6.Student";

    static {
        uM = new UriMatcher(UriMatcher.NO_MATCH);
        uM.addURI(AUTHORITY, "elements", ALL_ROWS);
        uM.addURI(AUTHORITY, "elements/#", ONE_ROW);
    }

    @Override
    public boolean onCreate() {
        dbOpenHelper = new StudentDBOpenHelper(getContext(), StudentDBOpenHelper.DATABASE_NAME, null, StudentDBOpenHelper.DATABASE_VERSION);
        return false;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteDatabase database;
        try {
            database = dbOpenHelper.getWritableDatabase();
        }
        catch (SQLiteException e) {
            database = dbOpenHelper.getReadableDatabase();
        }
        String groupBy = null;
        String having = null;
        SQLiteQueryBuilder squery = new SQLiteQueryBuilder();
        switch (uM.match(uri)) {
            case ONE_ROW :
                String idRow = uri.getPathSegments().get(1);
                squery.appendWhere(StudentDBOpenHelper.ID + "=" + idRow);
            default:
                break;
        }
        squery.setTables(StudentDBOpenHelper.TABLE);
        Cursor cursor = squery.query(database, projection, selection, selectionArgs, groupBy, having, sortOrder);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public  Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        SQLiteDatabase database;
        try {
            database = dbOpenHelper.getWritableDatabase();
        }
        catch (SQLiteException e) {
            database = dbOpenHelper.getReadableDatabase();
        }
        long id = database.insert(StudentDBOpenHelper.TABLE, null, values);
        return uri.buildUpon().appendPath(String.valueOf(id)).build();
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
