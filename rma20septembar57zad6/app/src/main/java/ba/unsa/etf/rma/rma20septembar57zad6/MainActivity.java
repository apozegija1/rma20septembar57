package ba.unsa.etf.rma.rma20septembar57zad6;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import static ba.unsa.etf.rma.rma20septembar57zad6.StudentContentProvider.AUTHORITY;
import static ba.unsa.etf.rma.rma20septembar57zad6.StudentContentProvider.ONE_ROW;

public class MainActivity extends AppCompatActivity {

    public EditText ime;
    public EditText index;
    public Button add;
    public static Uri uri = null;

    public ContentValues values = new ContentValues();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ime = findViewById(R.id.ime);
        index = findViewById(R.id.index);
        add = findViewById(R.id.add);

        uri = Uri.parse("content://" +AUTHORITY + "/elements" + ONE_ROW);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                values.put(StudentDBOpenHelper.ID, "0"); //zbog nemogućnosti da stignem ovako sam
                values.put(StudentDBOpenHelper.IME, ime.getText().toString());
                values.put(StudentDBOpenHelper.INDEKS, ime.getText().toString());
                insert();
            }
        });
    }

    private void insert() {
        ContentResolver cr = getApplicationContext().getContentResolver();
        cr.insert(uri, values);
        Toast.makeText(getApplicationContext(), "Uspjesno dodano u bazu RMAPopravni!", Toast.LENGTH_SHORT).show();
        ime.setText("");
        index.setText("");
    }
}