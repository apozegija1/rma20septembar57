package ba.unsa.etf.rma.rma20septembar57zad6;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class StudentDBOpenHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "RMAPopravni.db";
    public static final int DATABASE_VERSION = 1;


    public StudentDBOpenHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public StudentDBOpenHelper (Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public static final String TABLE = "students";
    public static final String INNER_ID = "_id";
    public static final String ID = "id";
    public static final String IME = "ime";
    public static final String INDEKS = "indeks";

    public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS students " +
            "( _id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "id INTEGER, " +
            "ime TEXT NOT NULL, " +
            "indeks TEXT NOT NULL);";

    public static final String DROP = "DROP TABLE IF EXISTS students;";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP);
        onCreate(db);
    }
}
