package ba.unsa.etf.rma.rma20septembar57zad6;

public class Student {
    private int id;
    private String ime;
    private String indeks;


    public Student(int id, String ime, String indeks) {
        this.id = id;
        this.ime = ime;
        this.indeks = indeks;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getIndeks() {
        return indeks;
    }

    public void setIndeks(String indeks) {
        this.indeks = indeks;
    }
}
