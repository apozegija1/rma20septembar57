package ba.unsa.etf.rma.rma20septembar57;


public class Weather {
    private String izlazak;
    private String zalazak;
    private String opis;

    public Weather(String izlazak, String zalazak, String opis) {
        this.izlazak = izlazak;
        this.zalazak = zalazak;
        this.opis = opis;
    }

    public Weather() {

    }

    public String getIzlazak() {
        return izlazak;
    }

    public void setIzlazak(String izlazak) {
        this.izlazak = izlazak;
    }

    public String getZalazak() {
        return zalazak;
    }

    public void setZalazak(String zalazak) {
        this.zalazak = zalazak;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }
}
