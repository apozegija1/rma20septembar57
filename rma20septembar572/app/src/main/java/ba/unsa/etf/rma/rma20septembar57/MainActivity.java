package ba.unsa.etf.rma.rma20septembar57;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements WeatherInteractor.IOnAction{

    public EditText izlazak;
    public EditText zalazak;
    public EditText opis;
    public Button update;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        izlazak = findViewById(R.id.izlazak);
        zalazak = findViewById(R.id.zalazak);
        opis = findViewById(R.id.opis);
        update = findViewById(R.id.edit);
        new WeatherInteractor((WeatherInteractor.IOnAction)this).execute("get");
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateWeather();
            }
        });
    }

    private void updateWeather() {
        new WeatherInteractor((WeatherInteractor.IOnAction)this).execute("get");
    }

    @Override
    public void setWeather(Weather weather) {
        izlazak.setText(weather.getIzlazak());
        zalazak.setText(weather.getZalazak());
        opis.setText(weather.getOpis());
    }
}