package ba.unsa.etf.rma.rma20septembar57;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class WeatherInteractor extends AsyncTask<String, Integer, Void> {

    private String baseUrl = "http://api.openweathermap.org/data/2.5/weather?q=Croatia&appid=d1833416cd6fd91921b462429662894f";

    public IOnAction caller;
    private Weather weather = new Weather();
    public interface IOnAction {
        void setWeather(Weather weather);
    }

    public WeatherInteractor(IOnAction caller) {
        this.caller = caller;
    }

    @Override
    protected Void doInBackground(String... strings) {
        try {
            URL url = new URL(baseUrl);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);
            JSONObject jo = new JSONObject(rezultat);
            JSONObject pom = (JSONObject) jo.get("sys");
            String izlazak = String.valueOf(pom.getLong("sunrise"));
            String zalazak = String.valueOf(pom.getLong("sunset"));

            JSONArray array = (JSONArray) jo.get("weather");
            pom = (JSONObject) array.get(0);
            weather = new Weather(izlazak, zalazak, pom.getString("description"));
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while((line = reader.readLine()) != null){
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }


    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        caller.setWeather(weather);
    }
}
